package ru.tsc.babeshko.tm.exception.entity;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}